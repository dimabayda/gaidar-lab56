﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Text;
using System.Threading.Tasks;

namespace RemoteServer
{
	class Program
	{
		static void Main(string[] args)
		{
			RemotingConfiguration.Configure("RemoteServer.exe.config", false);
			Console.WriteLine("Server is listening...");
			Console.ReadKey();
		}
	}
}
