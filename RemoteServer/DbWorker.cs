﻿using RemoteServer.Data;
using Shared;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace RemoteServer
{
	public class DbWorker : MarshalByRefObject, IDbWorker
	{
		private AppDataContext appDataContext;
		private SqlConnection sqlConnection;

		public DbWorker()
		{
			appDataContext = new AppDataContext();
			sqlConnection = appDataContext.Database.Connection as SqlConnection;
			Server server = GetServers()[0];
			CheckNewDatabases(server);
			CheckNewTables(server);
			CheckNewColumns(server);
		}

		public void AddFragmentedTable(FragmentedTable fragmentedTable)
		{
			appDataContext.FragmentedTables.Load();
			appDataContext.FragmentedTables.Add(fragmentedTable);
			appDataContext.SaveChanges();
		}

		public void DeleteFragmentedTable(FragmentedTable fragmentedTable)
		{
			int frId = fragmentedTable.Id;
			FragmentedTable dbFragment = appDataContext.FragmentedTables.First(fr => fr.Id == frId);
			appDataContext.FragmentedTables.Remove(dbFragment);
			appDataContext.SaveChanges();
		}

		public List<FragmentedTable> GetFragmentedTables()
		{
			appDataContext.FragmentedTables
				.Include(frg => frg.Fragments.Select(fr => fr.Columns))
				.Include(frg => frg.Fragments.Select(fr => fr.Conditions))
				.Include(frg => frg.Fragments.Select(fr => fr.Table))
				.Include(frg => frg.Fragments.Select(fr => fr.Database))
				.Load();
			var fragments = appDataContext.FragmentedTables.ToList();
			return fragments;
		}

		public List<Server> GetServers()
		{
			appDataContext.Servers
				.Include(server => server.Databases.Select(db => db.Tables.Select(tb => tb.Columns)))
				.Load();
			return appDataContext.Servers.ToList();
		}

		public FragmentTableData SelectFromFragmentedTable(FragmentedTable fragmentedTable)
		{
			FragmentTableData fragmentDataRows = new FragmentTableData();
			sqlConnection.Open();
			foreach (Fragment fragment in fragmentedTable.Fragments)
			{
				string command = fragment.BuildSelectQuery();
				SqlCommand sqlCommand = new SqlCommand(command, sqlConnection);
				SqlDataReader reader = sqlCommand.ExecuteReader();
				while (reader.Read())
				{
					int id = (int)reader["Id"];
					FragmentDataRow fragmentDataRow = new FragmentDataRow(id, fragment);
					for (int i = 0; i < reader.FieldCount; i++)
					{
						string columnName = reader.GetName(i);
						if (!columnName.Equals("Id"))
						{
							object columnValue = reader.GetValue(i);
							fragmentDataRow[columnName] = columnValue;
						}
					}
					fragmentDataRows.Add(fragmentDataRow);
				}
				reader.Close();
			}
			sqlConnection.Close();
			return fragmentDataRows;
		}

		public void ExecuteQuery(string query)
		{
			SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
			sqlConnection.Open();
			sqlCommand.ExecuteNonQuery();
			sqlConnection.Close();
		}

		private void CheckNewDatabases(Server server)
		{

			appDataContext.DatabaseViews.Load();
			var databases = appDataContext.DatabaseViews.ToList();
			var nonExistingDatabases = databases
				.Select(db => db.Name)
				.Except(server.Databases.Select(db => db.Name));
			if (nonExistingDatabases.Count() > 0)
			{
				foreach (string dbName in nonExistingDatabases)
				{
					server.Databases.Add(new Shared.Models.Database
					{
						Name = dbName,
						Tables = new List<Table>()
					});
				}
				appDataContext.SaveChanges();
			}
		}

		private void CheckNewTables(Server server)
		{
			List<TableProcRes> tables = appDataContext.Database
				.SqlQuery<TableProcRes>("procTables").ToList();
			var groupedTables = tables.GroupBy(table => table.DatabaseName);
			foreach (var grp in groupedTables)
			{
				Shared.Models.Database database = server.Databases.Single(db => db.Name == grp.Key);
				var nonExistingTables = grp
					.Select(tbl => tbl.Name)
					.Except(database.Tables.Select(tbl => tbl.Name));
				if (nonExistingTables.Count() > 0)
				{
					foreach (string tblName in nonExistingTables)
					{
						database.Tables.Add(new Table
						{
							Name = tblName,
							Columns = new List<Column>()
						});
					}
					appDataContext.SaveChanges();
				}
			}
		}

		private void CheckNewColumns(Server server)
		{
			List<ColumnProcRes> columns = appDataContext.Database
				.SqlQuery<ColumnProcRes>("procColumns").ToList();
			var groupedByDbColumns = columns.GroupBy(col => col.DatabaseName);
			foreach (var groupByDb in groupedByDbColumns)
			{
				Shared.Models.Database database = server.Databases
					.Single(db => db.Name == groupByDb.Key);
				var groupedByTableColumns = groupByDb.GroupBy(col => col.TableName);
				foreach (var groupByTable in groupedByTableColumns)
				{
					Table table = database.Tables.Single(tbl => tbl.Name == groupByTable.Key);
					var nonExistingColumns = groupByTable
						.Where(col => !col.ColumnName.Equals("Id"))
						.Select(tbl => (tbl.ColumnName, tbl.GetColumnType()))
						.Except(table.Columns.Select(col => (col.Name, col.Type)));
					if (nonExistingColumns.Count() > 0)
					{
						foreach ((string colName, ColumnType columnType) in nonExistingColumns)
						{
							table.Columns.Add(new Column
							{
								Name = colName,
								Type = columnType
							});
						}
						appDataContext.SaveChanges();
					}
				}
			}
		}

		public int GetFreeId(Fragment fragment)
		{
			string query = $"SELECT TOP 1 id FROM {fragment.GetTablePath()} ORDER BY id DESC";
			SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
			sqlConnection.Open();
			int id = (int)sqlCommand.ExecuteScalar();
			sqlConnection.Close();
			return id + 1;
		}
	}
}
