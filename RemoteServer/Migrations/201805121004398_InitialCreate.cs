namespace RemoteServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FragmentedTables",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Fragments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Database_Id = c.Int(),
                        Table_Id = c.Int(),
                        FragmentedTable_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Databases", t => t.Database_Id)
                .ForeignKey("dbo.Tables", t => t.Table_Id)
                .ForeignKey("dbo.FragmentedTables", t => t.FragmentedTable_Id)
                .Index(t => t.Database_Id)
                .Index(t => t.Table_Id)
                .Index(t => t.FragmentedTable_Id);
            
            CreateTable(
                "dbo.Columns",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Type = c.Int(nullable: false),
                        Fragment_Id = c.Int(),
                        Table_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Fragments", t => t.Fragment_Id)
                .ForeignKey("dbo.Tables", t => t.Table_Id)
                .Index(t => t.Fragment_Id)
                .Index(t => t.Table_Id);
            
            CreateTable(
                "dbo.Conditions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Operation = c.Int(nullable: false),
                        Value = c.String(),
                        Column_Id = c.Int(),
                        Fragment_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Columns", t => t.Column_Id)
                .ForeignKey("dbo.Fragments", t => t.Fragment_Id)
                .Index(t => t.Column_Id)
                .Index(t => t.Fragment_Id);
            
            CreateTable(
                "dbo.Databases",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Server_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Servers", t => t.Server_Id)
                .Index(t => t.Server_Id);
            
            CreateTable(
                "dbo.Tables",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Database_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Databases", t => t.Database_Id)
                .Index(t => t.Database_Id);
            
            CreateTable(
                "dbo.Servers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Databases", "Server_Id", "dbo.Servers");
            DropForeignKey("dbo.Fragments", "FragmentedTable_Id", "dbo.FragmentedTables");
            DropForeignKey("dbo.Fragments", "Table_Id", "dbo.Tables");
            DropForeignKey("dbo.Fragments", "Database_Id", "dbo.Databases");
            DropForeignKey("dbo.Tables", "Database_Id", "dbo.Databases");
            DropForeignKey("dbo.Columns", "Table_Id", "dbo.Tables");
            DropForeignKey("dbo.Conditions", "Fragment_Id", "dbo.Fragments");
            DropForeignKey("dbo.Conditions", "Column_Id", "dbo.Columns");
            DropForeignKey("dbo.Columns", "Fragment_Id", "dbo.Fragments");
            DropIndex("dbo.Tables", new[] { "Database_Id" });
            DropIndex("dbo.Databases", new[] { "Server_Id" });
            DropIndex("dbo.Conditions", new[] { "Fragment_Id" });
            DropIndex("dbo.Conditions", new[] { "Column_Id" });
            DropIndex("dbo.Columns", new[] { "Table_Id" });
            DropIndex("dbo.Columns", new[] { "Fragment_Id" });
            DropIndex("dbo.Fragments", new[] { "FragmentedTable_Id" });
            DropIndex("dbo.Fragments", new[] { "Table_Id" });
            DropIndex("dbo.Fragments", new[] { "Database_Id" });
            DropTable("dbo.Servers");
            DropTable("dbo.Tables");
            DropTable("dbo.Databases");
            DropTable("dbo.Conditions");
            DropTable("dbo.Columns");
            DropTable("dbo.Fragments");
            DropTable("dbo.FragmentedTables");
        }
    }
}
