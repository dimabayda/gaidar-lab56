namespace RemoteServer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCla : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Columns", "Fragment_Id", "dbo.Fragments");
            DropIndex("dbo.Columns", new[] { "Fragment_Id" });
            CreateTable(
                "dbo.ColumnFragments",
                c => new
                    {
                        Column_Id = c.Int(nullable: false),
                        Fragment_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Column_Id, t.Fragment_Id })
                .ForeignKey("dbo.Columns", t => t.Column_Id, cascadeDelete: true)
                .ForeignKey("dbo.Fragments", t => t.Fragment_Id, cascadeDelete: true)
                .Index(t => t.Column_Id)
                .Index(t => t.Fragment_Id);
            
            DropColumn("dbo.Columns", "Fragment_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Columns", "Fragment_Id", c => c.Int());
            DropForeignKey("dbo.ColumnFragments", "Fragment_Id", "dbo.Fragments");
            DropForeignKey("dbo.ColumnFragments", "Column_Id", "dbo.Columns");
            DropIndex("dbo.ColumnFragments", new[] { "Fragment_Id" });
            DropIndex("dbo.ColumnFragments", new[] { "Column_Id" });
            DropTable("dbo.ColumnFragments");
            CreateIndex("dbo.Columns", "Fragment_Id");
            AddForeignKey("dbo.Columns", "Fragment_Id", "dbo.Fragments", "Id");
        }
    }
}
