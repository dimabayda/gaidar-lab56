// <auto-generated />
namespace RemoteServer.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AddView : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddView));
        
        string IMigrationMetadata.Id
        {
            get { return "201805121052419_AddView"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
