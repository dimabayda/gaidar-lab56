﻿using Shared.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteServer.Data
{
	public class AppDataContext : DbContext
	{
		public DbSet<FragmentedTable> FragmentedTables { get; set; }
		public DbSet<Server> Servers { get; set; }
		public DbSet<DatabaseView> DatabaseViews { get; set; }

		public AppDataContext() : base("name=MetadataDb")
		{
		}
	}
}
