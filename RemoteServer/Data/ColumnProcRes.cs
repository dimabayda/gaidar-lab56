﻿using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteServer.Data
{
	public class ColumnProcRes
	{
		public string DatabaseName { get; set; }
		public string TableName { get; set; }
		public string ColumnName { get; set; }
		public string ColumnType { get; set; }

		public ColumnType GetColumnType()
		{
			return ColumnType.Equals("int") ? Shared.Models.ColumnType.Integer : Shared.Models.ColumnType.String;
		}
	}
}
