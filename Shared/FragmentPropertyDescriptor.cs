﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
	public class FragmentPropertyDescriptor : PropertyDescriptor
	{
		private readonly object value;

		public FragmentPropertyDescriptor(string key, object value) : base(key, new Attribute[0])
		{
			this.value = value;
		}

		public override Type ComponentType => typeof(FragmentDataRow);

		public override bool IsReadOnly => false;

		public override Type PropertyType => value.GetType();

		public override bool CanResetValue(object component) => true;

		public override void ResetValue(object component) { }

		public override bool ShouldSerializeValue(object component) => false;

		public override void SetValue(object component, object value)
		{
			FragmentDataRow fragmentDataRow = component as FragmentDataRow;
			fragmentDataRow[Name] = value;
		}

		public override object GetValue(object component)
		{
			FragmentDataRow fragmentDataRow = component as FragmentDataRow;
			return fragmentDataRow[Name];
		}
	}
}
