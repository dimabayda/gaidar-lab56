﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
	[Serializable]
	public class FragmentTableData : ObservableCollection<FragmentDataRow>, ITypedList
	{
		public event Action<FragmentDataRow> OnDataChanged;

		public FragmentTableData()
		{
			CollectionChanged += FragmentTableData_CollectionChanged;
		}

		public PropertyDescriptorCollection GetItemProperties(PropertyDescriptor[] listAccessors)
		{
			PropertyDescriptorCollection descriptors = new PropertyDescriptorCollection(null, false);
			foreach (var keyVal in this[0])
			{
				descriptors.Add(new FragmentPropertyDescriptor(keyVal.Key, keyVal.Value));
			}
			return descriptors;
		}

		public string GetListName(PropertyDescriptor[] listAccessors)
		{
			return nameof(FragmentTableData);
		}

		private void FragmentTableData_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			if (e.Action == NotifyCollectionChangedAction.Add)
			{
				foreach (object obj in e.NewItems)
				{
					FragmentDataRow fragmentDataRow = obj as FragmentDataRow;
					fragmentDataRow.PropertyChanged += FragmentDataRow_PropertyChanged;
				}
			}
			if (e.Action == NotifyCollectionChangedAction.Remove)
			{
				foreach (object obj in e.NewItems)
				{
					FragmentDataRow fragmentDataRow = obj as FragmentDataRow;
					fragmentDataRow.PropertyChanged -= FragmentDataRow_PropertyChanged;
				}
			}
		}

		private void FragmentDataRow_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			FragmentDataRow fragmentDataRow = sender as FragmentDataRow;
			OnDataChanged?.Invoke(fragmentDataRow);
		}
	}
}
