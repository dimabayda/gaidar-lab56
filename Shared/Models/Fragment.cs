﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models
{
	[Serializable]
	public class Fragment
	{
		public int Id { get; set; }
		public Database Database { get; set; }
		public Table Table { get; set; }
		public List<Column> Columns { get; set; }
		public List<Condition> Conditions { get; set; }

		public string BuildSelectQuery()
		{
			StringBuilder query = new StringBuilder();
			string builtColumns = GetColumns();
			query.Append($"SELECT Id {builtColumns} FROM {GetTablePath()}");
			if (Conditions != null && Conditions.Count > 0)
			{
				string builtQueries = string.Join(" AND ", Conditions.Select(cQuery => cQuery.Build()));
				query.Append($" WHERE {builtQueries}");
			}
			query.Append(";");
			return query.ToString();
		}

		public string BuildUpdateQuery(int id, IEnumerable<object> values)
		{
			if (Columns.Count > 0 && values.Count() != Columns.Count)
			{
				throw new Exception("Count must be equal or greater of zero!");
			}
			StringBuilder query = new StringBuilder();
			string setters = string.Join(", ", Columns
				.Zip(values, Tuple.Create)
				.Select(tuple => $"{tuple.Item1.Name} = {GetColumnValue(tuple.Item2)}"));
			query.Append($"UPDATE {GetTablePath()} SET {setters} WHERE Id = {id};");
			return query.ToString();
		}

		private string GetColumnValue(object value)
		{
			if (value is string str)
			{
				return $"'{str}'";
			}
			if (value is int num)
			{
				return $"{num}";
			}
			return null;
		}

		public string BuildInsertQuery(int id, IEnumerable<object> values)
		{
			if (values.Count() == 0)
			{
				List<object> newValues = new List<object>();
				foreach (Column col in Columns)
				{
					newValues.Add(col.GetDefaultValue());
				}
				values = newValues;
			}
			StringBuilder query = new StringBuilder();
			string builtColumns = GetColumns();
			string builtValues = GetValues(values);
			query.Append($"INSERT INTO {GetTablePath()}(Id {builtColumns}) VALUES ({id} {builtValues});");
			return query.ToString();
		}

		public string BuildDeleteQuery(int id)
		{
			return $"DELETE FROM {GetTablePath()} WHERE Id = {id};";
		}

		public string GetTablePath()
		{
			return $"{Database.Name}.dbo.{Table.Name}";
		}

		private string GetValues(IEnumerable<object> values)
		{
			if (values.Count() == 0)
			{
				return "";
			}
			return ", " + string.Join(", ", values.Select(val => GetColumnValue(val)));
		}

		private string GetColumns()
		{
			if (Columns.Count() == 0)
			{
				return "";
			}
			return ", " + string.Join(", ", Columns.Select(column => column.Name));
		}
	}
}
