﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models
{
	public enum ColumnType
	{
		Integer, String
	}

	[Serializable]
	public class Column : IEquatable<Column>
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public ColumnType Type { get; set; }
		public List<Fragment> Fragments { get; set; }

		public bool Equals(Column other)
		{
			if (other == null)
			{
				return false;
			}
			return Id == other.Id;
		}

		public object GetDefaultValue()
		{
			return Type == ColumnType.Integer ? (object)0 : " ";
		}
	}
}
