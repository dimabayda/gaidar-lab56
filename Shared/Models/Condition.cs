﻿using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models
{
	public enum CompOperation
	{
		LessThan, LessOrEqual, Equal, NotEqual, GreaterThan, GreaterOrEqual
	}

	[Serializable]
	public class Condition
	{
		public int Id { get; set; }
		public Column Column { get; set; }
		public CompOperation Operation { get; set; }
		public string Value { get; set; }

		public string Build()
		{
			return $"{Column.Name} {GetOperationCharacter()} {Value}";
		}

		private string GetOperationCharacter()
		{
			switch (Operation)
			{
				case CompOperation.Equal: return "=";
				case CompOperation.NotEqual: return "<>";
				case CompOperation.GreaterThan: return ">";
				case CompOperation.GreaterOrEqual: return ">=";
				case CompOperation.LessOrEqual: return "<=";
				case CompOperation.LessThan: return "<";
			}
			return "";
		}
	}
}
