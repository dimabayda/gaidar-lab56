﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models
{
	[Serializable]
	public class FragmentedTable
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public List<Fragment> Fragments { get; set; }
	}
}
