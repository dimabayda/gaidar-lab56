﻿using Shared.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
	[Serializable]
	public class FragmentDataRow :
		FragmentDataRowItemDescriptor,
		INotifyPropertyChanged,
		IEnumerable<KeyValuePair<string, object>>
	{
		public event PropertyChangedEventHandler PropertyChanged;

		private int id;
		private Fragment fragment;
		private Dictionary<string, object> data = new Dictionary<string, object>();

		public object this[string key]
		{
			get
			{
				return data[key];
			}
			set
			{
				if (!data.ContainsKey(key))
				{
					data.Add(key, value);
				}
				else
				{
					data[key] = value;
				}
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(key));
			}
		}

		public FragmentDataRow(int id, Fragment fragment)
		{
			this.id = id;
			this.fragment = fragment;
		}

		public string BuildInsertQuery() => fragment.BuildInsertQuery(id, data.Select(dt => dt.Value));

		public string BuildUpdateQuery() => fragment.BuildUpdateQuery(id, data.Select(dt => dt.Value));

		public string BuildDeleteQuery() => fragment.BuildDeleteQuery(id);

		public override PropertyDescriptorCollection GetProperties()
		{
			PropertyDescriptorCollection descriptors = new PropertyDescriptorCollection(null, false);
			foreach (var keyVal in data)
			{
				descriptors.Add(new FragmentPropertyDescriptor(keyVal.Key, keyVal.Value));
			}
			return descriptors;
		}

		public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
		{
			return data.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
