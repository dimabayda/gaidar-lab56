﻿using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
	public interface IDbWorker
	{
		List<Server> GetServers();
		List<FragmentedTable> GetFragmentedTables();
		void AddFragmentedTable(FragmentedTable fragmentedTable);
		void DeleteFragmentedTable(FragmentedTable fragmentedTable);
		FragmentTableData SelectFromFragmentedTable(FragmentedTable fragmentedTable);
		void ExecuteQuery(string query);
		int GetFreeId(Fragment fragment);
	}
}
