﻿using Shared.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.ViewModels
{
	public class SelectFragmentViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		public List<Fragment> Fragments { get; }
		public Fragment SelectedFragment { get; set; }

		public SelectFragmentViewModel(List<Fragment> fragments)
		{
			Fragments = fragments;
		}
	}
}
