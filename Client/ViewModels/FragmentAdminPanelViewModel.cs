﻿using Client.Utils;
using Shared;
using Shared.Models;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;

namespace Client.ViewModels
{
	public class FragmentAdminPanelViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		private IDbWorker dbWorker;
		private Server currentServer;
		private Database currentDatabase;
		private Table currentTable;

		public ICommand AddFragmentCommand { get; set; }
		public ICommand AddFragmentedTableCommand { get; set; }
		public ICommand DeleteFragmentCommand { get; set; }
		public ICommand DeleteFragmentedTableCommand { get; set; }
		public ICommand DeleteConditionCommand { get; set; }
		public ObservableCollection<Column> SelectedColumns { get; set; } = new ObservableCollection<Column>();
		public ObservableCollection<Condition> Conditions { get; set; } = new ObservableCollection<Condition>();
		public ObservableCollection<Fragment> Fragments { get; } = new ObservableCollection<Fragment>();
		public string FragmentName { get; set; }

		public Server CurrentServer
		{
			get => currentServer; set
			{
				currentServer = value;
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Databases)));
			}
		}

		public Database CurrentDatabase
		{
			get => currentDatabase; set
			{
				currentDatabase = value;
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Tables)));
			}
		}

		public Table CurrentTable
		{
			get => currentTable; set
			{
				currentTable = value;
				Conditions.Clear();
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Columns)));
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Conditions)));
			}
		}

		public List<FragmentedTable> FragmentedTables
		{
			get
			{
				var fragmentedTables = dbWorker.GetFragmentedTables();
				return fragmentedTables;
			}
		}

		public List<Server> Servers
		{
			get
			{
				var servers = dbWorker.GetServers();
				return servers;
			}
		}

		public List<Database> Databases
		{
			get
			{
				return CurrentServer?.Databases;
			}
		}

		public List<Table> Tables
		{
			get
			{
				return CurrentDatabase?.Tables;
			}
		}

		public List<Column> Columns
		{
			get
			{
				return CurrentTable?.Columns;
			}
		}

		public FragmentAdminPanelViewModel()
		{
			dbWorker = Activator.GetObject(typeof(IDbWorker), "tcp://localhost:5000/DbWorker") as IDbWorker;
			AddFragmentCommand = new SimpleActionCommand(AddNewFragment);
			DeleteFragmentedTableCommand = new SimpleActionCommand(DeleteFragmentedTable);
			DeleteFragmentCommand = new SimpleActionCommand(DeleteFragment);
			DeleteConditionCommand = new SimpleActionCommand(DeleteCondition);
			AddFragmentedTableCommand = new SimpleActionCommand(AddNewFragmentedTable);
		}

		private void AddNewFragment(object param)
		{
			Fragment fragment = new Fragment
			{
				Columns = SelectedColumns.ToList(),
				Conditions = Conditions.ToList(),
				Database = CurrentDatabase,
				Table = CurrentTable
			};
			Fragments.Add(fragment);
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Fragments)));
		}

		private void DeleteFragment(object param)
		{
			Fragment fragment = param as Fragment;
			Fragments.Remove(fragment);
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Fragments)));
		}

		private void AddNewFragmentedTable(object param)
		{
			if (CurrentDatabase != null && CurrentTable != null && SelectedColumns.Count > 0 && !string.IsNullOrWhiteSpace(FragmentName))
			{
				FragmentedTable fragmentedTable = new FragmentedTable
				{
					Name = FragmentName,
					Fragments = Fragments.ToList()
				};
				dbWorker.AddFragmentedTable(fragmentedTable);
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(FragmentedTables)));
			}
		}

		private void DeleteFragmentedTable(object param)
		{
			if (param is FragmentedTable fragmentedTable)
			{
				dbWorker.DeleteFragmentedTable(fragmentedTable);
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(FragmentedTables)));
			}
		}

		private void DeleteCondition(object param)
		{
			if (param is Condition condition)
			{
				Conditions.Remove(condition);
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Conditions)));
			}
		}
	}
}
