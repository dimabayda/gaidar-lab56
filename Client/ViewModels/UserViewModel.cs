﻿using Client.Utils;
using Client.Views;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Shared;
using Shared.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Input;

namespace Client.ViewModels
{
	public class UserViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		private IDbWorker dbWorker;

		public ICommand RunCommand { get; set; }
		public ICommand ReloadCommand { get; set; }
		public ICommand DeleteFragmentRowCommand { get; set; }
		public ICommand AddNewCommand { get; set; }
		public FragmentedTable SelectedFragment { get; set; }

		public List<FragmentedTable> Fragments
		{
			get
			{
				var fragments = dbWorker.GetFragmentedTables();
				return fragments;
			}
		}

		public FragmentTableData RunResult { get; set; }

		public UserViewModel()
		{
			dbWorker = Activator.GetObject(typeof(IDbWorker), "tcp://localhost:5000/DbWorker") as IDbWorker;
			RunCommand = new SimpleActionCommand(RunFragment);
			ReloadCommand = new SimpleActionCommand(Reload);
			AddNewCommand = new SimpleActionCommand(AddNew);
			DeleteFragmentRowCommand = new SimpleActionCommand(DeleteFragmentRow);
		}

		private void RunFragment(object obj)
		{
			if (SelectedFragment != null)
			{
				ReloadData();
			}
		}

		private void ReloadData()
		{
			RunResult = dbWorker.SelectFromFragmentedTable(SelectedFragment);
			RunResult.OnDataChanged += RunResult_OnDataChanged;
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(RunResult)));
		}

		private void RunResult_OnDataChanged(FragmentDataRow obj)
		{
			dbWorker.ExecuteQuery(obj.BuildUpdateQuery());
		}

		private void DeleteFragmentRow(object obj)
		{
			FragmentDataRow fragmentDataRow = obj as FragmentDataRow;
			dbWorker.ExecuteQuery(fragmentDataRow.BuildDeleteQuery());
			ReloadData();
		}

		private void Reload(object obj)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null));
		}

		private void AddNew(object obj)
		{
			SelectFragmentDialog selectFragmentDialog = new SelectFragmentDialog(SelectedFragment.Fragments);
			selectFragmentDialog.ShowDialog();
			Fragment fragment = selectFragmentDialog.SelectedFragment;
			if (fragment != null)
			{
				int freeId = dbWorker.GetFreeId(fragment);
				FragmentDataRow fragmentDataRow = new FragmentDataRow(freeId, fragment);
				dbWorker.ExecuteQuery(fragmentDataRow.BuildInsertQuery());
				ReloadData();
			}
		}
	}
}
