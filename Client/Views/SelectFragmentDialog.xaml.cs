﻿using Client.ViewModels;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client.Views
{
    /// <summary>
    /// Interaction logic for SelectFragmentDialog.xaml
    /// </summary>
    public partial class SelectFragmentDialog : Window
    {
		private SelectFragmentViewModel selectFragmentViewModel;

		public Fragment SelectedFragment
		{
			get
			{
				return selectFragmentViewModel.SelectedFragment;
			}
		}

		public SelectFragmentDialog(List<Fragment> fragments)
        {
            InitializeComponent();
			selectFragmentViewModel = new SelectFragmentViewModel(fragments);
			DataContext = selectFragmentViewModel;
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			if (selectFragmentViewModel.SelectedFragment != null)
			{
				Close();
			}
			else
			{
				MessageBox.Show("Please select fragment");
			}
		}
	}
}
