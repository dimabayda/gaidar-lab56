﻿using Client.ViewModels;
using Shared.Models;
using System.Windows.Controls;

namespace Client.Views
{
	/// <summary>
	/// Interaction logic for FragmentAdminPanel.xaml
	/// </summary>
	public partial class FragmentAdminPanel : UserControl
	{
		private FragmentAdminPanelViewModel viewModel;

		public FragmentAdminPanel()
		{
			InitializeComponent();
			viewModel = new FragmentAdminPanelViewModel();
			DataContext = viewModel;
			columnsList.SelectionChanged += ColumnsList_SelectionChanged;
		}

		private void ColumnsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			viewModel.SelectedColumns.Clear();
			foreach(Column column in columnsList.SelectedItems)
			{
				viewModel.SelectedColumns.Add(column);
			}
		}
	}
}
