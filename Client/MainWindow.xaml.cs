﻿using System.Runtime.Remoting;
using System.Windows;
using Microsoft.VisualBasic;

namespace Client
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
			RemotingConfiguration.Configure("Client.exe.config", false);
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			string pass = Interaction.InputBox("Please login", "Password");
			if (pass == "123")
			{
				FragmentAdminTab.IsEnabled = true;
			}
			else
			{
				MessageBox.Show("Wrong password!");
			}
		}
	}
}
